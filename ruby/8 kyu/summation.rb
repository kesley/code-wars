def summation(num)
  (1..num).to_a.sum
end
